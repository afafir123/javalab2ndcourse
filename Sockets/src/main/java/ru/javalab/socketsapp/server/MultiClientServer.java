package ru.javalab.socketsapp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MultiClientServer {
    private ServerSocket serverSocket;
    private List<ClientHandler> clients;

    public MultiClientServer(){
        clients = new ArrayList<ClientHandler>();
    }

    public void start (int port){
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        while (true){
            try {
                ClientHandler handler = new ClientHandler(serverSocket.accept());
                handler.start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }


    private class ClientHandler extends Thread{
        private Socket clientSocket;
        private BufferedReader reader;

        public ClientHandler(Socket clientSocket){
            this.clientSocket = clientSocket;
            clients.add(this);
            System.out.println("New client");
        }

        @Override
        public void run(){
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    for (ClientHandler client : clients) {
                        PrintWriter writer = new PrintWriter(client.clientSocket.getOutputStream(), true);
                        writer.println(line);
                    }
                }
                reader.close();
            } catch (IOException e) {
                throw new IllegalStateException();
            }
        }
    }
}
