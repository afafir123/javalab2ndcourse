package ru.javalab.socketsapp.cliients;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class GreetingClient {
    private Socket clientSocket;
    public void startConn (String ipAddres, int port){
        try {
            clientSocket = new Socket(ipAddres, port);
            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
            writer.println("Hello");
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

}


