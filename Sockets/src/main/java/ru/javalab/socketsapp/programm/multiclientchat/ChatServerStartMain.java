package ru.javalab.socketsapp.programm.multiclientchat;

import ru.javalab.socketsapp.server.MultiClientServer;

public class ChatServerStartMain {
    public static void main(String[] args) {
        MultiClientServer multiClientServer = new MultiClientServer();
        multiClientServer.start(7000);
    }
}
