package ru.javalab.socketsapp.programm.multiclientchat;

import ru.javalab.socketsapp.cliients.ChatClient;

import java.util.Scanner;

public class ChatClientMain {
    public static void main(String[] args) {
        ChatClient chatClient = new ChatClient();
        chatClient.startConnection("10.17.0.44", 12345);
        Scanner sc = new Scanner(System.in);
        while (true){
            String message = sc.nextLine();
            chatClient.sendMessage(message);
        }
    }
}
