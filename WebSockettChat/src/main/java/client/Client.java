package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Message;
import model.Response;
import model.UserMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Client {

    private Socket clientSocket;
    private PrintWriter writer;
    private BufferedReader reader;




    public void startConn (String ip, int port){
        try {
            clientSocket = new Socket(ip, port);
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            new Thread(recieveMessageTask).start();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String msg) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        UserMessage message = new UserMessage();
        String[] params = msg.substring(msg.indexOf(" ")+1).split(" ");
        message.setHeader(msg.split(" ")[0]);
        if (message.getHeader().equals("login")){
            message.setPayload(Map.of("login", params[0], "password", params[1] ));
        }
        if (message.getHeader().equals("message")){
            message.setPayload(Map.of("message", msg.substring(msg.indexOf(" ")+1)));
        }
        if (message.getHeader().equals("pagination")){
            message.setPayload(Map.of("page", params[0], "size", params[1]));
        }
        writer.println(mapper.writeValueAsString(message));
    }


    public void responseMatcher(UserMessage message){
        if (message.getHeader().equals("Pagination")){
            String msg =message.getPayload().get("msg");
            System.out.println(msg);
        }
        if (message.getHeader().equals("Message")){
            System.out.println(message.getPayload().get("msg"));
        }
        if (message.getHeader().equals("Connected")){
            if (Boolean.valueOf(message.getPayload().get("result"))){
            System.out.println("Connected");}
            else System.out.println("You are not conntected");
        }
        if (message.getHeader().equals("Error")){
            System.out.println(message.getPayload().get("error"));
        }

    }


    private Runnable recieveMessageTask = new Runnable() {
        @Override
        public void run() {
            while (true){
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    String message;
                    while ((message = reader.readLine()) != null) {
                        UserMessage userMessage = mapper.readValue(message, UserMessage.class);
                        responseMatcher(userMessage);
                    }
                }
                catch (IOException e){
                    throw new IllegalStateException(e);
                }
            }
        }
    };

}
