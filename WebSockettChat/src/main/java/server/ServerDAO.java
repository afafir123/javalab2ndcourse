package server;

import model.Message;
import model.User;

import java.sql.SQLException;
import java.util.List;

public interface ServerDAO {
    void addMessage(int id, String message);
    int addUser(User user) throws SQLException;
    int findUser(User user);
    List<Message> getMessages(int page, int size);
}
