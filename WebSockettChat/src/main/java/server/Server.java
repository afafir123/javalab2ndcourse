package server;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Message;
import model.Response;
import model.UserMessage;
import model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class Server {
    public static final String PATH_TO_PROPERTY = "D:\\db.properties";
    private static Connection conn;
    private static String url;
    private static String user;
    private static String password;
    private static ServerDAO dao = ServerDaoImpl.getInstance();
    private static List<ClientHandler> clients;
    private ObjectMapper mapper;

    public Server() {
        mapper = new ObjectMapper();
        clients = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
        try {
            FileInputStream propFile = new FileInputStream(PATH_TO_PROPERTY);
            Properties prop = new Properties();
            prop.load(propFile);
            url = prop.getProperty("url");
            user = prop.getProperty("user");
            password = prop.getProperty("password");
            conn = DriverManager.getConnection(url, user, password);
        } catch (IOException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void start(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                System.out.println("server working");
                Socket socket = serverSocket.accept();
                ClientHandler handler = new ClientHandler(socket);
                clients.add(handler);
                handler.start();

            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Connection getConn() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }


    class ClientHandler extends Thread {
        private Socket socket;
        private BufferedReader in;
        private BufferedWriter out;
        User user = new User();
        private int id;

        private ClientHandler(Socket socket) throws IOException {
            this.socket = socket;
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        }

        private void sendMessage(String str) throws IOException {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(str);
        }

        @Override
        public void run() {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    String line;
                    while ((line = in.readLine()) != null) {
                        UserMessage message = mapper.readValue(line, UserMessage.class);
                        switcher(message);
                    }
                    in.close();
                    socket.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
        }

        private void broadcast(String string) {
            for (ClientHandler clientHandler : clients) {
                PrintWriter writer1 = new PrintWriter(clientHandler.out, true);
                writer1.println(string);
            }
        }

        private void switcher(UserMessage message) throws IOException {
            String command = message.getHeader();
            switch (command) {
                case "login":
                    login(message);
                    break;
                case "logout":
                    logout(message);
                    break;
                case "message":
                    message(message);
                    break;
                case "pagination":
                    pagination(message);
                    break;
                 default:
                     error();
                     break;

            }
        }

        private void error () throws IOException {
            UserMessage response = new UserMessage();
            response.setHeader("Error");
            response.setPayload(Map.of("error", "Wrong command"));
            sendMessage(mapper.writeValueAsString(response));
            return;
        }
        private void login(UserMessage message) throws IOException {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            user.setLogin(message.getPayload().get("login"));
            user.setPassword(encoder.encode(message.getPayload().get("password")));
            int id = dao.findUser(user);
            if (id == -1) {
                try {
                    dao.addUser(user);
                    this.id = dao.findUser(user);
                } catch (SQLException e) {
                    UserMessage response = new UserMessage();
                    response.setHeader("Connected");
                    response.setPayload(Map.of("result", "false"));
                    sendMessage(mapper.writeValueAsString(response));
                    return;
                }
            }
            else this.id = id;
            UserMessage response = new UserMessage();
            response.setHeader("Connected");
            response.setPayload(Map.of("result", "true"));
            sendMessage(mapper.writeValueAsString(response));
        }

        private void message(UserMessage message) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            if (user.getLogin() == null) {
                Response response = new Response();
                response.setHeader("Connected");
                response.setData(new Object[]{false});
                sendMessage(mapper.writeValueAsString(response));
                return;
            }
            String msg = message.getPayload().get("message");
            System.out.println(msg);
            dao.addMessage(id, msg);
            msg = new Date()+ " " + user.getLogin()+":"+ msg;
            UserMessage response = new UserMessage();
            response.setHeader("Message");
            response.setPayload(Map.of("msg", msg));
            broadcast(mapper.writeValueAsString(response));
        }

        private void logout(UserMessage message) throws IOException {
            if (user.getLogin() == null) {
                sendMessage("Думаю, стоит залогиниться");
                return;
            }
            sendMessage("Вы покинули чат. Соединение сброшено");
            clients.remove(this);
            socket.close();
        }

        private void pagination(UserMessage message) throws IOException {
            int page = Integer.parseInt(message.getPayload().get("page"));
            int size = Integer.parseInt(message.getPayload().get("size"));
            List<Message> messages = dao.getMessages(page, size);
            for (Message msg: messages
                 ) {
                UserMessage response = new UserMessage();
                response.setHeader("Pagination");
                response.setPayload(Map.of("msg", msg.getDate()+ " " + msg.getId()+ " :" + msg.getMessage()));
                ObjectMapper mapper = new ObjectMapper();
                sendMessage(mapper.writeValueAsString(response));
            }

        }
    }
}
