package server;

import model.Message;
import model.User;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ServerDaoImpl implements ServerDAO {
    private static volatile ServerDaoImpl instance;
    public static ServerDaoImpl getInstance(){
        ServerDaoImpl localInstance =instance;
        if (localInstance == null){
            synchronized (ServerDaoImpl.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new ServerDaoImpl();
                }
            }
        }
        return localInstance;
    }

    @Override
    public void addMessage(int id,String message) {
        try (Connection conn = Server.getConn()){
            String query = "INSERT INTO message(userid, date, message) VALUES (?,?,?)";
            PreparedStatement ps =conn.prepareStatement(query);
            ps.setInt(1, id);
            ps.setTimestamp(2, Timestamp.from(Instant.now()));
            ps.setString(3, message);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public List<Message> getMessages(int page, int size) {
        List<Message> messages = new ArrayList<>();
        try (Connection conn = Server.getConn()){
            String query = "SELECT * FROM message LIMIT ? OFFSET ?";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, size);
            ps.setInt(2, (page-1)*size);
            ResultSet rslt = ps.executeQuery();
            while (rslt.next()){
                Message message = new Message();
                message.setId(rslt.getInt("userid"));
                message.setMessage(rslt.getString("message"));
                message.setDate(rslt.getTimestamp("date"));
                messages.add(message);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;

    }

    @Override
    public int addUser(User user) throws SQLException {
        int response = 0;
        try (Connection conn = Server.getConn()){
            String query = "INSERT INTO userlogin(login, password) VALUES (?,?)";
            PreparedStatement ps =conn.prepareStatement(query);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            response = ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
        return response;
    }

    @Override
    public int findUser(User user) {
        String query = "SELECT id FROM userlogin WHERE login = ? AND password = ? LIMIT 1";
        int id = -1;
        try(Connection conn = Server.getConn()) {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return id;
            }
            id = rs.getInt("id");
        } catch (SQLException e) {
            System.out.println(e);
        }
        return id;
    }
}
