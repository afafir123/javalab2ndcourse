package dto;

import models.Good;

public class GoodDto implements  Dto{
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Builder newBuilder() {
        return new GoodDto().new Builder();
    }

    public class Builder{
        private Builder(){
        }
        public Builder setId(int id) {
            GoodDto.this.id = id;
            return this;
        }

        public Builder setName(String name) {
            GoodDto.this.name = name;
            return this;
        }

        public GoodDto build() {
            return GoodDto.this;
        }
    }
}
