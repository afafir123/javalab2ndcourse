package dto;

import models.User;

public class UserDto implements Dto {
    private int id;
    private String login;
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static Builder newBuilder() {
        return new UserDto().new Builder();
    }

    public static UserDto from(User user) {
        return new UserDto()
                .newBuilder().setId(user.getId()).setLogin(user.getLogin()).setRole(user.getRole()).build();
    }

    public class Builder {
        private Builder() {
        }

        public Builder setId(int id) {
            UserDto.this.id = id;
            return this;
        }

        public Builder setLogin(String login) {
            UserDto.this.login = login;
            return this;
        }

        public Builder setRole(String role) {
            UserDto.this.role = role;
            return this;
        }

        public UserDto build() {
            return UserDto.this;
        }

    }
}
