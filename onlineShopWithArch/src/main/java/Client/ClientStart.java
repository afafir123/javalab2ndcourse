package Client;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Scanner;

public class ClientStart {
    public static void main(String[] args) throws JsonProcessingException {
        Scanner sc = new Scanner(System.in);
        Client client = new Client();
        client.startConn("127.0.0.1", 123);
        while (true) {
            String msg = sc.nextLine();
            client.sendMessage(msg);
        }
    }
}
