package Client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import protocol.Request;
import protocol.Response;
import protocol.json.Header;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

public class Client {
        private Socket clientSocket;
        private PrintWriter writer;
        private BufferedReader reader;
        private String token;

    public void sendMessage(String msg) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String command = msg.split(" ")[0];
        String[] params = msg.substring(msg.indexOf(" ")+1).split(" ");
        Request request = new Request();
        Header header = new Header();
        header.setCommand(command);
        if (token != null)
            header.setToken(token);
        request.setHeader(header);
        if (command.equals("login") ||  command.equals("register")){
            request.setPayload(Map.of("login", params[0], "password", params[1]));
        }
        if (command.equals("buy")|| command.equals("add")) {
            if (token == null){
                System.out.println("Авторизируйтесь");
                return;
            }
            request.setPayload(Map.of("name", params[0]));
        }
        writer.println(mapper.writeValueAsString(request));
    }
        public void startConn (String ip, int port){
            try {
                clientSocket = new Socket(ip, port);
                writer = new PrintWriter(clientSocket.getOutputStream(), true);
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                new Thread(recieveMessageTask).start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }


    private Runnable recieveMessageTask = new Runnable() {
        @Override
        public void run() {
            while (true){
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        Response message = mapper.readValue(line, Response.class);
                        responseHandler(message);
                    }
                }
                catch (IOException e){
                    throw new IllegalStateException(e);
                }
            }
        }
        void responseHandler(Response response){
            if(response.getHeader().equals("logined")){
                token = (String) response.getData();
                System.out.println("You are logined");
                System.out.println(token);
            }
            if (response.getHeader().equals("registred")){
                token = (String) response.getData();
                System.out.println("You are registred and logined");
                System.out.println(token);
            }
            if(response.getHeader().equals("error")){
                System.out.println((String) response.getData());
            }
            if (response.getHeader().equals("bought")){
                System.out.println("You bought "+(String)response.getData());
            }
            if (response.getHeader().equals("added")){
                System.out.println("You added "+(String)response.getData());
            }

        }
    };
}
