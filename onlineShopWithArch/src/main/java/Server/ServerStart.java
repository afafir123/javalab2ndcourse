package Server;

import context.ApplicationContext;
import context.ApplicationContextReflectionBased;
import org.reflections.Reflections;

public class ServerStart {
    public static void main(String[] args) {
        Reflections reflections = new Reflections();
        ApplicationContext context = new ApplicationContextReflectionBased();
        Server server = new Server();
        server.start(123);
    }
}
