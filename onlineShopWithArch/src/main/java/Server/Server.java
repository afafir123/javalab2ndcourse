package Server;

import com.fasterxml.jackson.databind.ObjectMapper;
import context.ApplicationContext;
import context.ApplicationContextReflectionBased;
import protocol.Request;
import protocol.RequestHandler;
import protocol.Response;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private static List<ClientHandler> clients;
    public static ApplicationContext context;

    public Server() {
        clients = new ArrayList<>();
        context = new ApplicationContextReflectionBased();
    }

    public void start(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                Socket socket = serverSocket.accept();
                ClientHandler handler = new ClientHandler(socket);
                clients.add(handler);
                handler.start();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    class ClientHandler extends Thread {
        private Socket socket;
        private BufferedReader in;
        private BufferedWriter out;
        private RequestHandler handler;


        private ClientHandler(Socket socket) throws IOException {
            this.socket = socket;
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            handler = new RequestHandler();
        }


        @Override
        public void run() {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String line;
                while ((line = in.readLine()) != null) {
                    Request request = mapper.readValue(line, Request.class);
                    sendMessage(handler.handleRequest(request));;
                }
                in.close();
                socket.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        private void sendMessage(Response response) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(mapper.writeValueAsString(response));
        }
    }
}
