package protocol;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import protocol.json.Header;

import java.util.Map;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {
    private Header header;
    private Map<String, String> payload;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Map<String, String> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, String> payload) {
        this.payload = payload;
    }

    public String getCommand() {
        return header.getCommand();
    }

    public String getParameter(String name) {
        return payload.get(name);
    }
}
