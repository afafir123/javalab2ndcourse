package protocol;

public class Response<T> {
    private String header;
    private T data;


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Response(){
        super();
    }

    public Response(String header, T data) {
        this.header = header;
        this.data = data;
    }

    public static <E> Response<E> build(String header, E data) {
        return new Response<>(header, data);
    }
}
