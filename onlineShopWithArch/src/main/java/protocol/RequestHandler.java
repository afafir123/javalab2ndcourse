package protocol;

import Server.Server;
import context.ApplicationContextReflectionBased;
import dto.Dto;
import dto.GoodDto;
import dto.UserDto;
import models.User;
import requestDispatcher.RequestDispatcher;
import services.AuthService;
import services.SignInService;

public class RequestHandler {
    private RequestDispatcher dispatcher;
    AuthService service;

    public RequestHandler() {
        this.dispatcher = new RequestDispatcher();
        this.service = Server.context.getComponent(AuthService.class, "authService");
    }

    // метод-реакция на запрос
    public Response<?> handleRequest(Request request) {
       if (request.getCommand().equals("login")){
           UserDto dto = (UserDto) dispatcher.doDispatch(request);
           if (dto.getRole() == null){
               return new Response<>("error", "You aren't logined");
           }

           return new Response("logined", service.buildToken((UserDto) dto));
       }
        if (request.getCommand().equals("register")){
            UserDto dto = (UserDto) dispatcher.doDispatch(request);
            if (dto.getRole() == null){
                return new Response<>("error", "You aren't logined");
            }
            AuthService service = new AuthService();
            return new Response("registred", service.buildToken((UserDto) dto));
        }

        if (request.getCommand().equals("add")){
            try {
                GoodDto dto = (GoodDto) dispatcher.doDispatch(request);
                if (dto.getName() == null){
                    return new Response("error", "this good already added");
                }
                else return new Response("added", dto.getName());
            }
            catch (IllegalStateException e){
                return new Response("error", "access error");
            }
        }

        if (request.getCommand().equals("buy")){
            try {
                GoodDto dto = (GoodDto) dispatcher.doDispatch(request);
                if (dto.getName() == null){
                    return new Response("error", "this good doesnt exist");
                }
                else return new Response("bought", dto.getName());
            }
            catch (IllegalStateException e){
                return new Response("error", "не удалось приобрести товар");
            }
        }
        
        return null;
    }
}
