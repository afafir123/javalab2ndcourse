package requestDispatcher;

import Server.Server;
import dto.Dto;
import dto.GoodDto;
import dto.UserDto;
import protocol.Request;
import services.AdminService;
import services.AuthService;
import services.SignInService;
import services.UserService;

public class RequestDispatcher {
    private SignInService signService;
    private UserService userService;
    private AdminService adminService;

    public RequestDispatcher() {
        this.signService = Server.context.getComponent(SignInService.class, "signInService");
        this.userService =  Server.context.getComponent(UserService.class, "userService");
        this.adminService = Server.context.getComponent(AdminService.class, "adminService");
    }

    public Dto doDispatch(Request request) {
        if (request.getCommand().equals("login")){
            try {
                return signService.login(request.getParameter("login"),request.getParameter("password"));
            }catch (IllegalStateException e){
                return new UserDto();
            }
        }
        if (request.getCommand().equals("register")) {
            try {
                return signService.register(request.getParameter("login"),request.getParameter("password"));
            } catch (IllegalStateException e) {
                return new UserDto();
            }
        }
        if (request.getCommand().equals("buy")) {
            try {
                return userService.buyGood(request.getHeader().getToken(), request.getParameter("name"));
            } catch (IllegalStateException e) {
                throw new IllegalStateException();
            }
        }
        if (request.getCommand().equals("add")){
            try {
                return adminService.addGood(request.getHeader().getToken(), request.getParameter("name"));
            } catch (IllegalStateException e){
                throw new IllegalStateException();
            }
        }
        return null;
    }
}
