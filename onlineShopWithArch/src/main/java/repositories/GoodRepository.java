package repositories;

import models.Good;
import models.User;

import java.util.Optional;

public interface GoodRepository extends CrudRepository<Good, Integer> {
    Optional<Good> findByName(String name);
    int addGood(String name);
    int deleteGood(String name);
}
