package repositories;

import context.Component;
import models.Good;
import utils.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class GoodRepositoryDaoImpl implements GoodRepository, Component {
    @Override
    public Optional<Good> findByName(String name) {
        String query = "SELECT * FROM good WHERE name = ?";
        Good good = new Good();
        try (Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            rs.next();
            good.setId(rs.getInt("id"));
            good.setName(rs.getString("name"));
            return Optional.ofNullable(good);
        } catch (SQLException e) {
            return  Optional.empty();
        }
    }

    @Override
    public int addGood(String name) {
        String query = "INSERT INTO good(name) values (?)";
        try (Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, name);
            int result = ps.executeUpdate();
            return result;
        } catch (SQLException e) {
            return  0;
        }
    }

    @Override
    public int deleteGood(String name) {
        return 0;
    }

    @Override
    public Optional<Good> findOne(Integer id) {
        String query = "SELECT * FROM good WHERE id = ?";
        Good good = new Good();
        try (Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            good.setId(rs.getInt("id"));
            good.setName(rs.getString("name"));
            return Optional.ofNullable(good);
        } catch (SQLException e) {
            return  Optional.empty();
        }
    }

    @Override
    public List<Good> findAll() {
        return null;
    }

    @Override
    public String getName() {
        return "goodRepository";
    }
}
