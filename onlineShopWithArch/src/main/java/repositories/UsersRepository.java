package repositories;

import models.User;

import java.sql.SQLException;
import java.util.Optional;

public interface UsersRepository extends CrudRepository<User, Integer> {
    int addUser(String login, String password) throws SQLException;
    Optional<User> findByLogin(String login);
}
