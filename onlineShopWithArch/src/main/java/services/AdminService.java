package services;

import context.Component;
import dto.GoodDto;
import dto.UserDto;
import repositories.GoodRepository;
import repositories.GoodRepositoryDaoImpl;

public class AdminService implements Component {
    private AuthService authService;

    private GoodRepository goodRepository;
    public GoodDto addGood(String token, String name){
        UserDto dto = authService.verifyToken(token);
        if (!dto.getRole().equals("admin")){
            throw new IllegalStateException();
        }
        int state = goodRepository.addGood(name);
        if (state == 1){
            return new GoodDto().newBuilder().setName(name).build();
        }
        else return new GoodDto();

    }

    @Override
    public String getName() {
        return "adminService";
    }
}
