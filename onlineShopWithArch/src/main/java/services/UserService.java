package services;

import context.Component;
import dto.Dto;
import dto.GoodDto;
import dto.UserDto;
import models.Good;
import models.User;
import repositories.GoodRepository;
import repositories.GoodRepositoryDaoImpl;

public class UserService implements Component {
    private GoodRepository goodRepository;
    private AuthService authService;
    public GoodDto buyGood(String token, String name){
        UserDto user = authService.verifyToken(token);
        if (user == null){
            throw new IllegalStateException();
        }
        Good good = goodRepository.findByName(name).orElse(null);
        if (good!= null) {
            GoodDto dto = GoodDto.newBuilder()
                    .setId(good.getId())
                    .setName(good.getName()).build();
            return dto;
        }
        return new GoodDto();


    }

    @Override
    public String getName() {
        return "userService";
    }
}
