package services;

import context.Component;
import dto.UserDto;
import models.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import protocol.Request;
import repositories.UsersRepository;
import repositories.UsersRepositoryDaoImpl;

import java.sql.SQLException;

public class SignInService implements Component {
    public UserDto login(String login, String password){
        UsersRepository repository = new UsersRepositoryDaoImpl<>();
        User user = repository.findByLogin(login).orElseThrow(IllegalStateException::new);
        if (user!=null && new BCryptPasswordEncoder().matches(password, user.getPassword())){
            return new UserDto().newBuilder()
                    .setRole(user.getRole())
                    .setLogin(user.getLogin())
                    .setId(user.getId()).build();
        }
        else throw new IllegalStateException();
    }

    public UserDto register(String login, String password) {
        UsersRepository repository = new UsersRepositoryDaoImpl<>();
        try {
            repository.addUser(login, password);
            return login(login, password);
        } catch (SQLException e) {
            throw new IllegalStateException();
        }

    }

    @Override
    public String getName() {
        return "signInService";
    }
}
