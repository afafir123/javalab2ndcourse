package services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import context.Component;
import dto.UserDto;
import models.User;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class AuthService implements Component {
    private static final String SECRET = "y1jWkcx52R";
    public String buildToken(UserDto userDto) {
        Algorithm algorithmHS = null;
        try {
            algorithmHS = Algorithm.HMAC256(SECRET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String token = JWT.create().withHeader(Map.of("alg", "HS256","typ","JWT"))
                .withClaim("login",userDto.getLogin())
                .withClaim("role",userDto.getRole())
                .withClaim("id", userDto.getId())
                .sign(algorithmHS);
        return token;
    }

    public UserDto verifyToken(String token){
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            UserDto user = UserDto.newBuilder()
                    .setId(jwt.getClaim("id").asInt())
                    .setLogin(jwt.getClaim("login").asString())
                    .setRole(jwt.getClaim("role").asString()).build();
            return user;
        } catch (JWTVerificationException exception){
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getName() {
        return "authService";
    }
}
