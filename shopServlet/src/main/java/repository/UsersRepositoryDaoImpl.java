package repository;

import context.Component;
import model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import utils.ConnectionUtil;
import utils.UserUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryDaoImpl<T, ID> implements UsersRepository, Component {
    @Override
    public int addUser(String login, String password) {
        String query = "INSERT INTO tableuser(login, password) VALUES (?,?)";
        int result;
        try (Connection conn = ConnectionUtil.getConnection()){
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, login);
            ps.setString(2, new BCryptPasswordEncoder().encode(password));
            result = ps.executeUpdate();
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        String query = "SELECT * FROM tableuser WHERE login=?";
        try (Connection conn = ConnectionUtil.getConnection()){
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            return Optional.ofNullable(UserUtil.setUser(rs));
        } catch (SQLException e) {
            return Optional.empty();
        }
    }



    @Override
    public Optional<User> findOne(Integer id) {
        String query = "SELECT * FROM tableuser WHERE id=?";
        User user;
        try(Connection conn = ConnectionUtil.getConnection()){
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return Optional.ofNullable(UserUtil.setUser(rs));
        } catch (SQLException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public String getName() {
        return "userRepository";
    }
}
