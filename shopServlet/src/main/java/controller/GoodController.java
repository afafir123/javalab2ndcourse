package controller;

import context.ApplicationContext;
import service.GoodService;
import service.SignService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/goods")
public class GoodController extends HttpServlet {
    GoodService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        Object rawAttribute = servletContext.getAttribute("componentsContext");
        ApplicationContext primitiveContext = (ApplicationContext) rawAttribute;
        this.service = primitiveContext.getComponent(GoodService.class, "goodService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getAttribute("user") != null) {
            req.setAttribute("goods", service.getAllGoods());
            req.getRequestDispatcher("/goods.jsp").forward(req, resp);
        } else req.getRequestDispatcher("/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
