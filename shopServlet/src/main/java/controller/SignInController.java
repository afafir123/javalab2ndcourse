package controller;

import context.ApplicationContext;
import model.User;
import service.SignService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/signin")
public class SignInController extends HttpServlet {

    SignService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        Object rawAttribute = servletContext.getAttribute("componentsContext");
        ApplicationContext primitiveContext = (ApplicationContext)rawAttribute;
        this.service = primitiveContext.getComponent(SignService.class, "signService");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        try {
            User user = service.signIn(login, password);
            if (user == null){

                req.getRequestDispatcher("/signIn.jsp").forward(req, resp);
            }
            else {
                req.getSession(true).setAttribute("user", user);
                req.getRequestDispatcher("/goods.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
