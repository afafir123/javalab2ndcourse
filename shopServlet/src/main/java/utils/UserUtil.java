package utils;

import model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserUtil {
    public static User setUser(ResultSet rs){
        try {
            rs.next();
            User user = new User();
            user.setPassword(rs.getString("password"));
            user.setId(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            return user;
        } catch (SQLException e) {
            return null;
        }
    }
}
