package service;

import context.Component;
import model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import repository.UsersRepository;
import repository.UsersRepositoryDaoImpl;

import java.sql.SQLException;

public class SignService implements Component {
    UsersRepository repository;
    public User signIn(String login, String password) throws SQLException {
        UsersRepository repository = new UsersRepositoryDaoImpl<>();
        User user = repository.findByLogin(login).orElseThrow(IllegalStateException::new);
        if (user!=null && new BCryptPasswordEncoder().matches(password, user.getPassword())) {
            return user;
        }
        else  return null;
    }

    public int signUp(String login, String password) throws SQLException {
        return repository.addUser(login, password);
    }

    @Override
    public String getName() {
        return "signService";
    }
}
