package service;

import context.Component;
import model.Good;
import repository.GoodRepository;

import java.util.List;

public class GoodService implements Component {

    GoodRepository goodRepository;

    public List<Good> getAllGoods(){
        return goodRepository.findAll();
    }

    @Override
    public String getName() {
        return "goodService";
    }
}
