package main.java.ru.java_lab;


import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashSet;

public class Main {



    public static void main(String[] args) {
        start(args);
    }

    public static void start(String[] args){
        if (args.length == 0) {
            System.err.println("Nothing to download");
            System.exit(2);
        }
        args=getUniqueUrls(args);
        setThreads(args);

    }
    public static void setThreads(String[] args){
        for (int i = 0; i < args.length; i++) {
            try {
                URL u = decodeURL(args[i]);
                DownloadThread downloadThread = new DownloadThread(u);
                downloadThread.start();
            } catch (MalformedURLException e) {
                System.err.println("URL format exception. File: " + args[i]);}
        }




    }
    public static String[] getUniqueUrls(String[] urls){
        return new LinkedHashSet<String>(Arrays.asList(urls)).toArray(new String[0]);
    }
    public static URL decodeURL (String path ) throws MalformedURLException {
        String toReturn = null;
        try {
            toReturn = java.net.URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return new URL(toReturn);
    }
}
