package main.java.ru.java_lab;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DownloadThread extends Thread {
    private URL url;
    private static int downloaded = 0;
    private ReentrantLock lock = new ReentrantLock();

    public DownloadThread(URL url) {
        this.url = url;
    }

    @Override
    public void run() {
        int count;
        try (ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
             FileOutputStream outputStream = new FileOutputStream(getDestination())) {
            lock.lock();
            downloaded++;
            count = downloaded;
            lock.unlock();
            outputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            System.out.println("Downloaded " + count + " file");
        } catch (FileNotFoundException e) {
            System.err.println("Can't create with this name. File " + downloaded + " didn't load");
            throw new RuntimeException();
        } catch (IOException e) {
            System.err.println("whoops, IO exception. File " + downloaded + " didn't load");
            throw new RuntimeException();
        }


    }

    private String getDestination() {
        String afterLastSlash = url.toString().substring(url.toString().lastIndexOf('/') + 1);
        if (afterLastSlash.contains(".")) {
            return Thread.currentThread().getName() + afterLastSlash.substring(afterLastSlash.lastIndexOf('.'));
        } else return Thread.currentThread().getName() + ".html";
    }

    /*public void incDownloaded() {
        synchronized (locker) {
            downloaded++;
        }

    }
*/


}
