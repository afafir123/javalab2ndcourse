package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.JSON.Header;
import model.JSON.UserMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

    public class Client {
        private Socket clientSocket;
        private PrintWriter writer;
        private BufferedReader reader;
        private String token;


        public void startConn (String ip, int port){
            try {
                clientSocket = new Socket(ip, port);
                writer = new PrintWriter(clientSocket.getOutputStream(), true);
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                new Thread(recieveMessageTask).start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

    public void sendMessage(String msg) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String command = msg.split(" ")[0];
        String[] params = msg.substring(msg.indexOf(" ")+1).split(" ");
        Header header = new Header();
        UserMessage message = new UserMessage();
        if (command.equals("login")) {
            if (token    != null){
                System.out.println("Вы уже вошли");
                return;
            }
            String login = params[0];
            String password = params[1];
            header.setCommand("login");
            message.setHeader(header);
            message.setPayload(Map.of("login", login, "password", password));
        }
        if (command.equals("buy")){
            if (token == null){
                System.out.println("Нужно войти");
                return;
            }
            String name = params[0];
            header.setCommand("buy");
            header.setToken(token);
            message.setHeader(header);
            message.setPayload(Map.of("name", name));
        }
        if (command.equals("getAll")){
            if (token == null){
                System.out.println("Нужно войти");
                return;
            }
            header.setCommand("getAll");
            header.setToken(token);
            message.setHeader(header);
        }
        if (command.equals("add")){
            if (token == null){
                System.out.println("Нужно войти");
                return;
            }
            String name = params[0];
            header.setCommand("add");
            header.setToken(token);
            message.setHeader(header);
            message.setPayload(Map.of("name", name));
        }

        writer.println(mapper.writeValueAsString(message));
    }

    public void responseMatcher(UserMessage message){
        if (message.getHeader().getCommand().equals("Successful")) {
            token = message.getPayload().get("response");
            System.out.println("Вы успешно вошли");
        }
        if (message.getHeader().getCommand().equals("Bought")) {
            System.out.println(message.getPayload().get("response"));
        }
        if (message.getHeader().getCommand().equals("Error")) {
            System.out.println(message.getPayload().get("response"));
        }
        if (message.getHeader().getCommand().equals("anotherGood")) {
            System.out.println(message.getPayload().get("response"));
        }
        if (message.getHeader().getCommand().equals("added")) {
            System.out.println(message.getPayload().get("response"));
        }
    }


    private Runnable recieveMessageTask = new Runnable() {
        @Override
        public void run() {
            while (true){
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        UserMessage message = mapper.readValue(line, UserMessage.class);
                        responseMatcher(message);
                    }
                }
                catch (IOException e){
                    throw new IllegalStateException(e);
                }
            }
        }
    };

}
