package server;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Good;
import model.JSON.Header;
import model.JSON.UserMessage;
import server.exception.RightException;
import server.exception.UserNotFoundException;
import server.service.AdminService;
import server.service.AuthService;
import server.service.UserService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Server {
    public static final String PROPERTIES = "C:\\propeties\\onlineShop.properties";
    private static String dbUrl;
    private static String dbUser;
    private static String dbPassword;
    private static List<ClientHandler> clients;
    private ObjectMapper mapper;
    private Connection conn;

    public Server() {
        mapper = new ObjectMapper();
        clients = new ArrayList<>();
        FileInputStream propFile = null;
        try {
            propFile = new FileInputStream(PROPERTIES);
            Properties prop = new Properties();
            prop.load(propFile);
            dbUrl = prop.getProperty("url");
            dbUser = prop.getProperty("user");
            dbPassword = prop.getProperty("password");
            conn = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void start(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                Socket socket = serverSocket.accept();
                ClientHandler handler = new ClientHandler(socket);
                clients.add(handler);
                handler.start();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Connection getConn() throws SQLException {
        return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
    }


    class ClientHandler extends Thread {
        private Socket socket;
        private BufferedReader in;
        private BufferedWriter out;


        private ClientHandler(Socket socket) throws IOException {
            this.socket = socket;
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        }

        @Override
        public void run() {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String line;
                while ((line = in.readLine()) != null) {
                    UserMessage message = mapper.readValue(line, UserMessage.class);
                    switcher(message);
                }
                in.close();
                socket.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }

        private void switcher(UserMessage message) throws IOException {
            String command = message.getHeader().getCommand();
            switch (command){
                case "register":
                    try {
                        int response = AuthService.register(message);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } ;
                case "login":
                    try {
                        String token = AuthService.login(message);
                        if (token == null) sendResponse("Error", "User not found");
                        else sendResponse("Successful", token);
                    } catch (SQLException e) {
                        sendResponse("Error","Something went wrong");
                    }
                    break;
                case "buy":
                    try {
                        String token = message.getHeader().getToken();
                        String name = UserService.buy(token, message.getPayload().get("name"));
                        if (name == null){
                            sendResponse("Error","Товар не найден");
                        }
                        else sendResponse("Bought", "Товар успешно куплен");
                    } catch (SQLException e) {
                        sendResponse("Error","Something went wrong");;
                    }
                    break;
                case "getAll":
                    try {
                        String token = message.getHeader().getToken();
                        List <Good> goods = UserService.getAll(token);
                        if (goods == null){
                            sendResponse("Error","Товары не найдены");
                        }
                        for (Good good:goods){
                                sendResponse("anotherGood", good.getName());
                        }

                    } catch (SQLException e) {
                        sendResponse("Error","Something went wrong");
                    }
                    break;
                case "add":
                    try {
                        String name = message.getPayload().get("name");
                        String token = message.getHeader().getToken();
                        int resp = AdminService.add(token, name);
                        if (resp == 1){
                            sendResponse("added", "Товар успешно добавлен");
                        }
                    } catch (UserNotFoundException e) {
                        sendResponse("Error",e.getMessage());
                    } catch (RightException e) {
                        sendResponse("Error",e.getMessage());
                    }
                    break;
            }
        }


        private void sendMessage(String str) throws IOException {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(str);
        }
        private void sendResponse(String responseHead, String text) throws IOException{
            UserMessage response = new UserMessage();
            Header header = new Header();
            header.setCommand(responseHead);
            response.setHeader(header);
            response.setPayload(Map.of("response", text));
            sendMessage(mapper.writeValueAsString(response));
        }
    }


}

