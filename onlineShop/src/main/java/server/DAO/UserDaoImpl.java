package server.DAO;

import model.User;
import server.Server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao {
    private static volatile UserDaoImpl instance;
    public static UserDaoImpl getInstance(){
        UserDaoImpl localInstance =instance;
        if (localInstance == null){
            synchronized (UserDaoImpl.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new UserDaoImpl();
                }
            }
        }
        return localInstance;
    }

    @Override
    public User findUser(String login, String password) throws SQLException {
        String query = "SELECT id, role FROM login WHERE login = ? AND password = ? LIMIT 1";
        try (Connection connection = Server.getConn()){
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()){
                return null;
            }
            User user = new User(); user.setLogin(login); user.setPassword(password); user.setId(rs.getInt("id")); user.setRole(rs.getString("role"));
            return user;
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public int addUser(String login, String password) throws SQLException {
        String query = "INSERT INTO login(login, password) VALUES(?,?)";
        int response = 0;
        try (Connection conn = Server.getConn()){
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, login);
            ps.setString(2, password);
            response = ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
        return response;
    }

}
