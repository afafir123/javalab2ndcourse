package server.DAO;

import model.Good;

import java.sql.SQLException;
import java.util.List;

public interface GoodDao {
    Good findGood(String name) throws SQLException;
    int addGood(String name);
    List<Good> getGoods() throws SQLException;
}
