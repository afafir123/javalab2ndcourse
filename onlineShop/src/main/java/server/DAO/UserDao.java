package server.DAO;

import model.User;

import java.sql.SQLException;

public interface UserDao {
    User findUser(String login, String password) throws SQLException;
    int addUser(String login, String password) throws SQLException;

}
