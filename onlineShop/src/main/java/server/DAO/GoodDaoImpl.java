package server.DAO;

import model.Good;
import server.Server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodDaoImpl implements GoodDao {
    private static volatile GoodDaoImpl instance;
    public static GoodDaoImpl getInstance(){
        GoodDaoImpl localInstance =instance;
        if (localInstance == null){
            synchronized (GoodDaoImpl.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new GoodDaoImpl();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Good findGood(String name) throws SQLException {
        String query = "SELECT * FROM good where name = ?";
        try (Connection conn = Server.getConn()){
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()){
                return null;
            }
            Good good = new Good();
            good.setId(rs.getInt("id"));
            good.setName(name);
            return good;
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public int addGood(String name) {
        int response = 0;
        String query = "INSERT INTO good(name) VALUES (?)";
        try (Connection connection = Server.getConn()){
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, name);
            response = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public List<Good> getGoods() throws SQLException {
        List<Good> goods = new ArrayList<>();
        String query = "SELECT * FROM good";
        try (Connection connection = Server.getConn()){
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Good good = new Good();
                good.setId(rs.getInt("id"));
                good.setName(rs.getString("name"));
                goods.add(good);
            }
            return goods;
        } catch (SQLException e) {
            throw e;
        }
    }
}
