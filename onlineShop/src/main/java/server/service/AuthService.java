 package server.service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.JSON.JWT.JwtHeader;
import model.JSON.JWT.JwtMessage;
import model.JSON.JWT.JwtPayload;
import model.JSON.UserMessage;
import model.User;
import org.apache.commons.codec.binary.Hex;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import server.DAO.UserDao;
import server.DAO.UserDaoImpl;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Base64;



public class AuthService {
    public static final String SECRET = "y1jWkcx52R";
    public static String login(UserMessage message) throws SQLException, JsonProcessingException {
        User user;
        UserDao dao = new UserDaoImpl();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user = dao.findUser(message.getPayload().get("login"), encoder.encode(message.getPayload().get("password")));
        if (user != null){
            JwtHeader header = new JwtHeader();
            JwtPayload payload = new JwtPayload();
            payload.setLogin(user.getLogin());
            payload.setRole(user.getRole());
            JwtMessage jwt = new JwtMessage();
            jwt.setHeader(header);
            jwt.setPayload(payload);
            String token = buildToken(jwt);
            return token;
        }
        else return null;
    }


//todo
    public static int register(UserMessage message) throws SQLException, JsonProcessingException{
        int response = 0;
        UserDao dao = new UserDaoImpl();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return response;
    }


    private static String buildToken(JwtMessage jwt) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        byte[] header = mapper.writeValueAsBytes(jwt.getHeader());
        byte[] payload = mapper.writeValueAsBytes(jwt.getPayload());
        String codedHeader = new String (Base64.getEncoder().encode(header));
        String codedPayload = new String(Base64.getEncoder().encode(payload));
        String signature = encode(SECRET, codedHeader+"."+codedPayload);
        return codedHeader+"."+codedPayload+"."+signature;

    }
        private static String encode(String key, String data) {
            try {
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                return new String(Hex.encodeHex(sha256_HMAC.doFinal(data.getBytes("UTF-8"))));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            return null;
        }
    public static User authenticate (String token) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String[] tokens = token.split("\\.");
        String header = tokens[0];
        String payload = tokens[1];
        String signature = tokens[2];
        if (!checkSignature(header, payload, signature))
            return null;
        payload = new String (Base64.getDecoder().decode(payload));
        JwtPayload jwtPayload = mapper.readValue(payload, JwtPayload.class);
        String role = jwtPayload.getRole();
        String login = jwtPayload.getLogin();
        User user = new User();
        user.setRole(role);
        user.setLogin(login);
        return user;
    }
    private static boolean checkSignature (String header, String payload, String signature){
        return signature.equals(encode(SECRET, header+"."+payload));

    }
}
