package server.service;

import model.User;
import server.DAO.GoodDao;
import server.DAO.GoodDaoImpl;
import server.exception.RightException;
import server.exception.UserNotFoundException;

import java.io.IOException;

public class AdminService {
    public static int add(String token, String name) throws IOException, RightException, UserNotFoundException {
        User user = AuthService.authenticate(token);
        if (user == null) {
            throw new UserNotFoundException();
        }
        if (user.getRole() != "admin") {
            throw new RightException();
        }
        GoodDao dao = GoodDaoImpl.getInstance();
        int result = dao.addGood(name);
        return result;
    }

}
