package server.service;

import model.Good;
import model.User;
import server.DAO.GoodDao;
import server.DAO.GoodDaoImpl;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class UserService {
    public static String buy(String token, String name) throws IOException, SQLException {
      User user =AuthService.authenticate(token);
      if (user == null){
          return null;
      }
        GoodDao dao = GoodDaoImpl.getInstance();
      Good good = dao.findGood(name);
      if(good == null){
          return null;
      }
      return name;
    }
    public static List<Good> getAll(String token) throws IOException, SQLException {
        User user =AuthService.authenticate(token);
        if (user == null){
            return null;
        }
        GoodDao dao = GoodDaoImpl.getInstance();
        List<Good> goods = dao.getGoods();
        return goods;
    }
}
