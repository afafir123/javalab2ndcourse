package server.exception;

public class RightException extends Exception {
    @Override
    public String getMessage() {
        return "You dont have access";
    }
}
