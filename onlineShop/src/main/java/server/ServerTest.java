package server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.JSON.Header;
import model.JSON.UserMessage;
import model.User;
import server.DAO.UserDao;
import server.DAO.UserDaoImpl;
import server.service.AuthService;

import java.sql.SQLException;
import java.util.Map;

public class ServerTest {
    public static void main(String[] args) throws SQLException, JsonProcessingException {
        Server start = new Server();
        start.start(1234);
    }
}
