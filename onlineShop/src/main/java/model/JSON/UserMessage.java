package model.JSON;

import java.util.Map;

public class UserMessage {
    private Header header;
    private Map<String, String> payload;

    public Map<String, String> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, String> params) {
        this.payload = params;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

}
