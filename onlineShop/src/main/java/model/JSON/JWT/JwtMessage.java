package model.JSON.JWT;

public class JwtMessage {
    private JwtHeader header;
    private JwtPayload payload;

    public JwtHeader getHeader() {
        return header;
    }

    public void setHeader(JwtHeader header) {
        this.header = header;
    }

    public JwtPayload getPayload() {
        return payload;
    }

    public void setPayload(JwtPayload payload) {
        this.payload = payload;
    }
}
