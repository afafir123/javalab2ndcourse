package ru.javalab.mywebapplication;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import main.java.ru.java_lab.Main;


public class MyCLass extends  HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/download.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] urls = req.getParameter("urls").split(" ");
        Main.start(urls);
        req.getRequestDispatcher("/download.jsp").forward(req,resp);
    }
}
