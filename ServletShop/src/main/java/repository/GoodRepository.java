package repository;

import model.Good;

import java.util.Optional;

public interface GoodRepository extends CrudRepository<Good, Integer> {
    Optional<Good> findByName(String name);
    int addGood(String name);
    int deleteGood(String name);
}
